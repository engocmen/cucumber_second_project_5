package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

import java.util.List;

public class SmartBearLoginBasePage {

    public SmartBearLoginBasePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = "input[id*='username']")
    public WebElement usernameInputBox;

    @FindBy(css = "input[id*='password']")
    public WebElement passwordInputBox;

    @FindBy(css = "input[id*='login']")
    public WebElement loginButton;

    @FindBy(css = "span[id*='status']")
    public WebElement statusMessage;








}

package pages;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SmartBearWebOrdersPage extends SmartBearLoginBasePage{

public SmartBearWebOrdersPage(){super();}

    @FindBy(css = "ul[id*='menu']>li")
    public List<WebElement> loginMenuItems;

    @FindBy(css = "[id*='btn']")
    public List<WebElement> WebViewAllOrdersButtons;

    @FindBy(css = "input[id*='OrderSelector']")
    public List<WebElement> checkBoxesList;

    @FindBy(css = "div[id*='orderMessage']")
    public WebElement deletedAllOrdersMessage;

    @FindBy(css = "select[name*='Product']>option")
    public List<WebElement> selectProductList;

    @FindBy(css = "li>input")
    public List<WebElement> inputAreaList;

    @FindBy(css = "input[id*='cardList']")
    public List<WebElement> cardList;

    @FindBy(css = "a[class*='btn']")
    public WebElement processButton;

    @FindBy(css = "table[id*='MainContent'] tr>td")
    public List<WebElement> confirmOrderList;


}









package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;
import pages.SmartBearLoginBasePage;
import pages.SmartBearWebOrdersPage;
import utils.Driver;

public class WebOrdersLoginSteps {
    WebDriver driver;
    SmartBearLoginBasePage smartBearLoginBasePage;
    SmartBearWebOrdersPage smartBearWebOrdersPage;


    @Before
    public void setup() {
        driver = Driver.getDriver();
        smartBearLoginBasePage = new SmartBearLoginBasePage();
        smartBearWebOrdersPage = new SmartBearWebOrdersPage();
    }


    @When("user enters username as {string}")
    public void user_enters_username_as(String string) {
       smartBearLoginBasePage.usernameInputBox.sendKeys(string);
    }

    @When("user enters password as {string}")
    public void user_enters_password_as(String string) {
        smartBearLoginBasePage.passwordInputBox.sendKeys(string);
    }

    @When("user clicks on Login button")
    public void user_clicks_on_Login_button() {
        smartBearLoginBasePage.loginButton.click();
    }

    @Then("user should see {string} message")
    public void user_should_see_message(String string) {
        Assert.assertEquals(smartBearLoginBasePage.statusMessage.getText(), string);
    }

    @Then("user should be routed to {string}")
    public void user_should_be_routed_to(String URL) {
        Assert.assertEquals(URL, driver.getCurrentUrl());
    }

    @Then("validate below menu items are displayed")
    public void validate_below_menu_items_are_displayed(DataTable dataTable) {
        for (int i = 0; i < dataTable.width(); i++) {
            Assert.assertEquals(dataTable.asList().get(i), smartBearWebOrdersPage.loginMenuItems.get(i).getText());
        }

    }

    @When("user clicks on {string} button")
    public void user_clicks_on_button(String buttons) {
        switch (buttons) {
            case "Check All":
                smartBearWebOrdersPage.WebViewAllOrdersButtons.get(0).click();
                break;
            case "Uncheck All":
                smartBearWebOrdersPage.WebViewAllOrdersButtons.get(1).click();
                break;
            case "Delete Selected":
                smartBearWebOrdersPage.WebViewAllOrdersButtons.get(2).click();
                break;
            case "Process":
                smartBearWebOrdersPage.processButton.click();
                break;
            default:
                throw new NotFoundException("The button is not defined!");
        }}

    @Then("all rows should be checked")
    public void all_rows_should_be_checked() {
        for (int i = 0; i < smartBearWebOrdersPage.checkBoxesList.size(); i++) {
            Assert.assertTrue(smartBearWebOrdersPage.checkBoxesList.get(i).isSelected());
        }
    }

    @Then("all rows should be unchecked")
    public void all_rows_should_be_unchecked() {
        for (int i = 0; i < smartBearWebOrdersPage.checkBoxesList.size(); i++) {
            Assert.assertFalse(smartBearWebOrdersPage.checkBoxesList.get(i).isSelected());
        }
    }

    @Then("validate all orders are deleted from the {string}")
    public void validate_all_orders_are_deleted_from_the(String string) {
        if ("List of All Orders".equals(string)) {
            Assert.assertTrue(smartBearWebOrdersPage.checkBoxesList.isEmpty());
        } else {
            throw new NotFoundException("The sentence is not defined!");
        }
    }


    @Then("validate user sees {string} message")
    public void validate_user_sees_message(String string) {
        Assert.assertEquals(string, smartBearWebOrdersPage.deletedAllOrdersMessage.getText());
    }

    @When("user clicks on {string} menu item")
    public void user_clicks_on_menu_item(String menuItem) {
        switch (menuItem) {
            case "View all orders":
                smartBearWebOrdersPage.loginMenuItems.get(0).click();
                break;
            case "View all products":
                smartBearWebOrdersPage.loginMenuItems.get(1).click();
                break;
            case "Order":
                smartBearWebOrdersPage.loginMenuItems.get(2).click();
                break;
            default:
                throw new NotFoundException("The Menu Item is not defined!");
        }}


    @When("user selects {string} as product")
    public void user_selects_as_product(String product) {
        switch (product) {
            case "MyMoney":
                smartBearWebOrdersPage.selectProductList.get(0).click();
                break;
            case "FamilyAlbum":
                smartBearWebOrdersPage.selectProductList.get(1).click();
                break;
            case "ScreenSaver":
                smartBearWebOrdersPage.selectProductList.get(2).click();
                break;
            default:
                throw new NotFoundException("The Menu Item is not defined!");
        }}


    @When("user enters {int} as quantity")
    public void user_enters_as_quantity(Integer int1) {
        smartBearWebOrdersPage.inputAreaList.get(0).sendKeys(""+int1);
    }

    @When("user enters all address information")
    public void user_enters_all_address_information() {
        smartBearWebOrdersPage.inputAreaList.get(5).sendKeys("John Doe");
        smartBearWebOrdersPage.inputAreaList.get(6).sendKeys("123 Anywhere Street");
        smartBearWebOrdersPage.inputAreaList.get(7).sendKeys("Any Town");
        smartBearWebOrdersPage.inputAreaList.get(8).sendKeys("Illinois");
        smartBearWebOrdersPage.inputAreaList.get(9).sendKeys("60666");
    }

    @When("user enters all payment information")
    public void user_enters_all_payment_information() {
        smartBearWebOrdersPage.cardList.get(0).click();
        smartBearWebOrdersPage.inputAreaList.get(10).sendKeys("1234567812345678");
        smartBearWebOrdersPage.inputAreaList.get(11).sendKeys("01/30");
    }

    @Then("user should see their order displayed in the {string} table")
    public void user_should_see_their_order_displayed_in_the_table(String string) {
        if ("List of All Orders".equals(string)) {
            Assert.assertFalse(smartBearWebOrdersPage.checkBoxesList.isEmpty());
        } else {
            throw new NotFoundException("The sentence is not defined!");
        }}

    @Then("validate all information entered displayed correct with the order")
    public void validate_all_information_entered_displayed_correct_with_the_order() {
        Assert.assertEquals("John Doe", smartBearWebOrdersPage.confirmOrderList.get(1).getText());
        Assert.assertEquals("FamilyAlbum", smartBearWebOrdersPage.confirmOrderList.get(2).getText());
        Assert.assertEquals("2", smartBearWebOrdersPage.confirmOrderList.get(3).getText());
        Assert.assertEquals("123 Anywhere Street", smartBearWebOrdersPage.confirmOrderList.get(5).getText());
        Assert.assertEquals("Any Town", smartBearWebOrdersPage.confirmOrderList.get(6).getText());
        Assert.assertEquals("Illinois", smartBearWebOrdersPage.confirmOrderList.get(7).getText());
        Assert.assertEquals("60666", smartBearWebOrdersPage.confirmOrderList.get(8).getText());
        Assert.assertEquals("Visa", smartBearWebOrdersPage.confirmOrderList.get(9).getText());
        Assert.assertEquals("1234567812345678", smartBearWebOrdersPage.confirmOrderList.get(10).getText());
        Assert.assertEquals("01/30", smartBearWebOrdersPage.confirmOrderList.get(11).getText());
    }


}
